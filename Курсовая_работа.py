import tkinter as tk
from tkinter import ttk
import sqlite3


class Main(tk.Frame):
    def __init__(self,root):
        super().__init__(root)
        self.init_main()
        self.db = db
        self.view_records()
#Glawny okno
    def init_main(self):
        toolbar=tk.Frame(bg='#dbdce2',bd=2)
        toolbar.pack(side=tk.TOP,fill=tk.X)
#картинки
        self.add_img=tk.PhotoImage(file='icon1.png')
        btn_open_dialog=tk.Button(toolbar,text='Собрать свой компютер',command=self.open_dialog,bg='#dbdce2',bd=0
                                  ,compound=tk.TOP,image=self.add_img)
        btn_open_dialog.pack(side=tk.LEFT)

        self.delete_img = tk.PhotoImage(file='icon-2.png')
        btn_delete = tk.Button(toolbar, text='Удалить список', command=self.delete_records, bg='#dbdce2', bd=0
                                    , compound=tk.TOP, image=self.delete_img)
        btn_delete.pack(side=tk.LEFT)

        self.search_img = tk.PhotoImage(file='icon-3.png')
        btn_search = tk.Button(toolbar, text='Поиск', command=self.open_search_dialog, bg='#dbdce2', bd=0
                               , compound=tk.TOP, image=self.search_img)
        btn_search.pack(side=tk.LEFT)


        self.tree=ttk.Treeview(self,columns=('ID','description','costs','total'),height=15,show='headings')

        self.tree.column('ID',width=250,anchor=tk.CENTER)
        self.tree.column('description',width=250,anchor=tk.CENTER)
        self.tree.column('costs',width=250,anchor=tk.CENTER)
        self.tree.column('total',width=250,anchor=tk.CENTER)
        #self.tree.column('ram',width=250, anchor=tk.CENTER)
        #self.tree.column('psu',width=250, anchor=tk.CENTER)

        self.tree.heading('ID',text='ID')
        self.tree.heading('description',text='Процессор')
        self.tree.heading('costs',text='Видеокарта')
        self.tree.heading('total',text='Жесткий диск')
        #self.tree.heading('ram', text='Ram')
        #self.tree.heading('psu', text='Блок питаня')

        self.tree.pack()

    def records(self,description,costs,total):
        self.db.insert_data(description,costs,total)
        self.view_records()



    def view_records(self):
        self.db.c.execute('''SELECT * FROM Computers''')
        [self.tree.delete(i) for i in self.tree.get_children()]
        [self.tree.insert('','end',values=row) for row in self.db.c.fetchall()]

    def delete_records(self):
        for selection_item in self.tree.selection():
            self.db.c.execute('''DELETE FROM Computers WHERE id=?''',(self.tree.set(selection_item,'#1'),))
        self.db.conn.commit()
        self.view_records()

    def search_records(self,description):
        description=('%'+description+'%',)
        self.db.c.execute('''SELECT * FROM Computers WHERE description LIKE ?''',description)
        [self.tree.delete(i)for i in self.tree.get_children()]
        [self.tree.insert('','end',values=row)for row in self.db.c.fetchall()]

    def open_dialog(self):
        Child()


    def open_search_dialog(self):
        Search()



class Child(tk.Toplevel):
    def __init__(self):
        super().__init__(root)
        self.init_create()
        self.view = app

#1-njy okno create komp
    def init_create(self):
        self.title('sobirat swoy kompyuter')
        self.geometry('500x350+400+300')
        self.resizable(False,False)

        label_cpu=tk.Label(self,text="Процессор(CPU)")
        label_cpu.place(x=30,y=40)

        label_gpu=tk.Label(self, text="Видеокарта")
        label_gpu.place(x=30, y=70)

        label_Disk=tk.Label(self, text="Жесткий диск")
        label_Disk.place(x=30, y=100)


        label_Ram=tk.Label(self, text="Ram")
        label_Ram.place(x=30, y=130)

        label_psu = tk.Label(self, text="Блок питания(PSU)")
        label_psu.place(x=30, y=160)


        self.cpu=ttk.Combobox(self,values=("Intel Core i3","Intel Core i5","Intel Core i7","Intel Core i9","AMD Radeon"))
        self.cpu.place(x=200,y=40)

        self.gpu = ttk.Combobox(self, values=("Nvidia GeForce GT450-2Gb", "GeForce GTX 1660-8Gb",
                                              "GeForce RTX3090Ti-24Gb","AMD Radeon"))
        self.gpu.place(x=200, y=70)

        self.hdd= ttk.Combobox(self, values=("HDD=250Gb","HDD=500Gb","HDD=1Tb", "SSD=250Gb","SSD=500Gb","SSD=1Tb"))
        #self.hdd.current(0)
        self.hdd.place(x=200, y=100)

        self.ram=ttk.Combobox(self, values=("2Gb","4Gb","6Gb","8Gb","16Gb","24Gb","128Gb","256Gb"))
        self.ram.place(x=200, y=130)

        self.psu=ttk.Combobox(self, values=("450W","550W","750W","1000W","1600W"))
        self.psu.place(x=200, y=160)

        btn_cancel=ttk.Button(self,text='Закрыть',command=self.destroy)
        btn_cancel.place(x=300,y=200)

        self.btn_ok=ttk.Button(self,text="Добавить")
        self.btn_ok.place(x=220,y=200)
        self.btn_ok.bind('<Button-1>',lambda event: self.view.records(self.cpu.get(),self.gpu.get(),self.hdd.get()))


        self.grab_set()
        self.focus_set()

class Search(tk.Toplevel):
    def __init__(self):
        super().__init__()
        self.init_search()
        self.view=app

    def init_search(self):
        self.title("Поиск")
        self.geometry('300x100+400+300')
        self.resizable(False,False)

        label_search=tk.Label(self,text='Поиск')
        label_search.place(x=50,y=20)

        self.entry_search=ttk.Entry(self)
        self.entry_search.place(x=105,y=20,width=150)

        btn_cancel=ttk.Button(self,text='Закрыть',command=self.destroy)
        btn_cancel.place(x=185,y=50)

        btn_search=ttk.Button(self,text="Поиск")
        btn_search.place(x=105,y=50)
        btn_search.bind('<Button-1>',lambda event:self.view.search_records(self.entry_search.get()))
        btn_search.bind('<Button-1>',lambda event:self.destroy(),add='+')



#База данных:
class DB:
    def __init__(self):
        self.conn=sqlite3.connect('Computers.db')
        self.c = self.conn.cursor()
        self.c.execute(
            '''CREATE TABLE IF NOT EXISTS Computers (id integer primary key,description text,costs text, total real)''')
        self.conn.commit()

    def insert_data(self,description,costs,total):
        self.c.execute('''INSERT INTO Computers (description,costs,total) VALUES (?, ?, ?) ''',
                        (description,costs,total))

        self.conn.commit()

if __name__ =="__main__":
    root=tk.Tk()
    db=DB()
    app=Main(root)
    app.pack()
    root.title('Computer Service')
    root.geometry('1200x720')
    root.mainloop()